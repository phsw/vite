/**
 *
 * @file src/interface/Interval_select.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Thibault Soucarre
 * @author Augustin Degomme
 *
 * @date 2024-07-17
 */
/*!
 *\file Interval_select.hpp
 */

#ifndef INTERVAL_SELECT_HPP
#define INTERVAL_SELECT_HPP

class Interval_select;

/* For moc compilation */
#include <string>
#include <map>
#include <list>
/* -- */
#include <QWidget>
#include "ui_interval_select.h"

#include "trace/values/Values.hpp"
#include "trace/EntityValue.hpp"
#include "trace/EntityTypes.hpp"
#include "trace/Entitys.hpp"
#include "trace/tree/Interval.hpp"
#include "trace/Trace.hpp"

#include "interface/Interface_graphic.hpp"
#include "interface/RangeSliderWidget.hpp"

/* -- */

/*!
 * \class Node select
 * \brief Class used to select which containers should be displayed
 *
 */

class Interval_select : public QDialog, protected Ui::interval_selector
{

    Q_OBJECT
    friend class Interface_graphic;

private:
    Interface_graphic *_interface_graphic;
    RangeSliderWidget _range_slider;
    int _range_span;

    /*!
     * \brief Maximum time reachable by the slider
     */
    double _max_possible_value;

    /*!
     * \brief Boolean preventing _max_possible_value changing when the slider is moved
     */
    bool _is_slider_moving;

public:
    /*!
     * Default constructor
     * \param parent The parent widget of the window.
     */
    Interval_select(Interface_graphic *interface_graphic);

    ~Interval_select() override;

    /*!
     * \brief Initialize slider bounds based on the interface trace
     */
    void init();

    /*!
     * \brief Apply changes to the render
     */
    void apply_settings();

    /*!
     * \brief Update the values of spinbox and slider with the ones of the render
     * Called if a zoom is triggered outside of the window, when the render zoom state is changed
     */
    void update_values();

private Q_SLOTS:
    void on_minSpinBox_valueChanged(double value);
    void on_maxSpinBox_valueChanged(double value);
    void on_sliderMoved(int low, int high);

    void on_reset_button_clicked();

    void on_auto_refresh_box_stateChanged();
};

#endif // INTERVAL_SELECT_HPP
