/**
 *
 * @file src/interface/resource.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Olivier Lagrasse
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
/*!
 *\file interface/resource.hpp
 *\brief This file gives some defines for the interface classes.
 */

#ifndef INTERFACE_RESOURCE_HPP
#define INTERFACE_RESOURCE_HPP

/*!
 * \brief Checks if a function return -1 as a value.
 *
 * This macro is used with C functions to check them if an error occurs. Thus, it display the file, the line and some informations (with m variable) of the error than exit the program.
 */
#define CKF(f, m)                                                        \
    if ((f) == -1) {                                                     \
        cerr << "File " << __FILE__ << " - line : " << __LINE__ << endl; \
        cerr << m << endl;                                               \
        exit(EXIT_FAILURE);                                              \
    }

/*!
 * \brief Checks if a function return NULL as a value.
 *
 * This macro is used with C functions to check them if an error occurs. Thus, it display the file, the line and some informations (with m variable) of the error than exit the program.
 */
#define CKFP(f, m)                                                       \
    if ((f) == NULL) {                                                   \
        cerr << "File " << __FILE__ << " - line : " << __LINE__ << endl; \
        cerr << m << endl;                                               \
        exit(EXIT_FAILURE);                                              \
    }

#endif
