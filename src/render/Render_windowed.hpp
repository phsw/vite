/**
 *
 * @file src/render/Render_windowed.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Nolan Bredel
 *
 * @date 2024-07-17
 */

#ifndef VITE_RENDER_WINDOWED_HPP
#define VITE_RENDER_WINDOWED_HPP

#include "Render.hpp"

class RenderLayout;

/**
 * \brief Allow to render on a QT window
 */
class Render_windowed : public Render
{

public:
    /**
     * \brief Get the widget associated to the renderer to display in the window
     * @return
     */
    virtual QWidget *get_render_widget() = 0;

    /**
     * \brief Set the RenderLayout in which the render is
     * \param layout new layout pointer value
     */
    virtual void set_layout(RenderLayout *layout) = 0;

    /**
     * Get the last frame displayed and save it on a QT image
     * @return
     */
    virtual QImage grab_frame_buffer() = 0;

    /*!
     * \brief This function draws the trace.
     */
    virtual bool build() = 0;

    /*!
     * \brief This function releases the trace.
     */
    virtual bool unbuild() = 0;

    virtual void release() = 0;

    virtual void show_minimap() = 0;

    /*!
     * \brief Refresh scroll bar positions when shortcuts execute movements
     */
    virtual void refresh_scroll_bars() = 0;

    /*!
     * \brief Zoom along the x axis between two values
     * \param begin The beginning time of the interval
     * \param end The end time of the interval
     */
    virtual void apply_zoom_on_interval(const Element_pos &begin, const Element_pos &end) = 0;

    /*!
     * \brief Change the percentage taken by container display in the render area.
     * \param view_size The new percentage (between 0 to 100).
     */
    virtual void change_container_scale_x(const int &view_size) = 0;

    /*!
     * \brief Change the scale of the y state drawing.
     * \param scale_coeff The new coefficient scale value to add to the current scale.
     */
    virtual void change_scale_y(const Element_pos &scale_coeff) = 0;

    /*!
     * \brief Replace the current scale by a new scale. (horizontally)
     * \param new_scale The new scale value to replace the current scale.
     */
    virtual void replace_scale_x(Element_pos new_scale) = 0;

    /*!
     * \brief Replace the current y translate by a new translate.
     * \param new_translate The new translate value to replace the current translate.
     */
    virtual void replace_translate_y(const Element_pos &new_translate) = 0;

    /*!
     * \brief Pre registered translation values (for x or y translate).
     * \param id The pre registered translation id.
     */
    virtual void registered_translate(int id) = 0;

    /*!
     * \brief Change the scale of state drawing.
     * \param scale_coeff The new coefficient scale value to add to the current scale.
     */
    virtual void change_scale_x(const Element_pos &scale_coeff) = 0;

    /*!
     * \brief Replace the current scale by a new scale. (vertically)
     * \param new_scale The new scale value to replace the current scale.
     */
    virtual void replace_scale_y(const Element_pos &new_scale) = 0;

    /*!
     * \brief Replace the current x translate by a new translate.
     * \param new_translate The new translate value to replace the current translate.
     */
    virtual void replace_translate_x(const Element_pos &new_translate) = 0;

    /**
     * \brief Updates the widget
     */
    virtual void update_render() = 0;

    /*!
     * \brief Getter for scroll_margin_x
     */
    virtual const int &get_scroll_margin_x() const = 0;

    /*!
     * \brief Getter for scroll_margin_y
     */
    virtual const int &get_scroll_margin_y() const = 0;
};

#endif // VITE_RENDER_WINDOWED_HPP
