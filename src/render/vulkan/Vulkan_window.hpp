/**
 *
 * @file src/render/vulkan/Vulkan_window.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Lucas Guedon
 *
 * @date 2024-07-17
 */
#ifndef VITE_VULKAN_WINDOW_H
#define VITE_VULKAN_WINDOW_H

#include "core/Core.hpp"
#include <QVulkanWindow>

class Render_vulkan;

class Vulkan_window : public QVulkanWindow
{
private:
    Interface_graphic *_interface_graphic;
    QVulkanInstance _inst;
    Render_vulkan *_main_renderer;

public:
    VkDeviceMemory buf_mem_uniform = VK_NULL_HANDLE;

    Vulkan_window(Interface_graphic *interface_graphic, Render_vulkan *main_renderer);
    QVulkanWindowRenderer *createRenderer() override;
};

#include "render/vulkan/Render_vulkan.hpp"

#endif // VITE_VULKAN_WINDOW_H
