/**
 *
 * @file src/render/vulkan/Vk_pipeline.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Lucas Guedon
 *
 * @date 2024-07-17
 */

#ifndef VK_PIPELINE_HPP
#define VK_PIPELINE_HPP

#include <QVulkanWindow>
#include <QVulkanDeviceFunctions>

class Vk_pipeline
{

protected:
    VkDevice _dev;
    QVulkanDeviceFunctions *_dev_funcs;
    VkPipeline _pipeline = VK_NULL_HANDLE;

public:
    Vk_pipeline();
    ~Vk_pipeline();

    void init(VkDevice dev, QVulkanDeviceFunctions *dev_funcs, const QVulkanWindow *window,
              const VkShaderModule &vert_shader, const VkShaderModule &frag_shader,
              const VkPipelineCache &cache, const VkPipelineLayout &pipeline_layout,
              const VkPipelineVertexInputStateCreateInfo *vertex_input, VkPrimitiveTopology topology);

    void bind_pipeline(VkCommandBuffer *command_buffer);

    void free_pipeline();
};

#endif