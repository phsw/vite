/**
 *
 * @file src/trace/LinkType.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Thibault Soucarre
 * @author Kevin Coulomb
 * @author Augustin Degomme
 * @author Clement Vuchener
 *
 * @date 2024-07-17
 */
#include <string>
#include <list>
#include <map>
/* -- */
#include "trace/values/Values.hpp"
#include "trace/ContainerType.hpp"
#include "trace/EntityType.hpp"
#include "trace/EntityValue.hpp"
#include "trace/LinkType.hpp"
/* -- */
using namespace std;

LinkType::LinkType(const Name &name,
                   ContainerType *container_type,
                   ContainerType *source_type,
                   ContainerType *destination_type,
                   map<std::string, Value *> opt) :
    EntityType(_EntityClass_Link, name, container_type, opt),
    _source_type(source_type),
    _destination_type(destination_type) {
    add_value(new EntityValue(name, this, opt));
}

LinkType::~LinkType() {
    // All the ContainerType are deleted by the trace itself (recursively)
    // We just set the pointers to NULL
    _source_type = nullptr;
    _destination_type = nullptr;
}

const ContainerType *LinkType::get_source_type() const {
    return _source_type;
}
const ContainerType *LinkType::get_destination_type() const {
    return _destination_type;
}
void LinkType::set_source_type(ContainerType *source) {
    _source_type = source;
}
void LinkType::set_destination_type(ContainerType *dest) {
    _destination_type = dest;
}
