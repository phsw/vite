/**
 *
 * @file src/trace/Entitys.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#ifndef ENTITYS_HPP
#define ENTITYS_HPP

#include <list>
#include <vector>
#include <stack>

#include "trace/values/Values.hpp"
#include "trace/Entity.hpp"
#include "trace/Event.hpp"
#include "trace/State.hpp"
#include "trace/StateChange.hpp"
#include "trace/Link.hpp"
#include "trace/Variable.hpp"
#include "trace/Container.hpp"

#endif
