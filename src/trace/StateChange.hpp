/**
 *
 * @file src/trace/StateChange.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Thibault Soucarre
 * @author Kevin Coulomb
 * @author Augustin Degomme
 * @author Clement Vuchener
 *
 * @date 2024-07-17
 */
#ifndef STATECHANGE_HPP
#define STATECHANGE_HPP

#include "trace/State.hpp"
/* -- */
#include "trace/tree/BinaryTree.hpp"

/*!
 * \class StateChange
 */
template <typename E>
class BinaryTree;
class State;
class StateChange
{
public:
    typedef std::list<StateChange *> Vector;
    typedef std::list<StateChange *>::const_iterator VectorIt;
    typedef BinaryTree<StateChange> Tree;

private:
    Date _time;
    State *_left = nullptr;
    State *_right = nullptr;

public:
    StateChange();
    StateChange(Date time);
    StateChange(Date time, State *left, State *right = nullptr);

    void set_right_state(State *right);
    Date get_time() const;
    State *get_left_state();
    State *get_right_state();
    const State *get_left_state() const;
    const State *get_right_state() const;
    ~StateChange();

    void clear();
};

#endif
