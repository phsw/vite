/**
 *
 * @file src/trace/LinkType.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Thibault Soucarre
 * @author Kevin Coulomb
 * @author Augustin Degomme
 * @author Clement Vuchener
 *
 * @date 2024-07-17
 */
#ifndef LINKTYPE_HPP
#define LINKTYPE_HPP

/*!
 * \file LinkType.hpp
 */

/*!
 * \class LinkType
 * \brief Describe the link types
 */
class LinkType : public EntityType
{
private:
    ContainerType *_source_type, *_destination_type;

public:
    /*!
     * \fn LinkType(const Name &name, ContainerType *container_type, ContainerType *source_type, ContainerType *destination_type, std::map<std::string, Value *> opt)
     * \brief Constructor
     * \param name Name of type
     * \param container_type Type of ancestor container
     * \param source_type Type of the source container
     * \param destination_type Type of the destination container
     * \param opt optional data of the link
     */
    LinkType(const Name &name, ContainerType *container_type, ContainerType *source_type, ContainerType *destination_type, std::map<std::string, Value *> opt);
    // LinkType();
    /*!
     * \fn ~LinkType()
     * \brief Destructor
     */
    ~LinkType() override;

    /*
    Thes getters and setters are useful for serialization
    */
    const ContainerType *get_source_type() const;
    const ContainerType *get_destination_type() const;
    void set_source_type(ContainerType *source);
    void set_destination_type(ContainerType *dest);
};

#endif
