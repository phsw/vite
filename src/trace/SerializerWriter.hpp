/**
 *
 * @file src/trace/SerializerWriter.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Augustin Degomme
 * @author Thibault Soucarre
 *
 * @date 2024-07-17
 */

#ifndef EVTBUILDER_HPP
#define EVTBUILDER_HPP

#include <QThread>
#include <QMutex>
#include <QWaitCondition>
#include <QSemaphore>
#include <queue>
#include <trace/IntervalOfContainer.hpp>
#include <trace/Serializer.hpp>

/*!
 * \file SerializerWriter.hpp
 * Thread that handles the serialization of an IntervalOfContainer
 */

class SerializerWriter : public QObject, public Singleton<SerializerWriter>
{
    Q_OBJECT

private:
    QWaitCondition *_cond;
    QSemaphore *_freeSlots;
    QSemaphore *_itcProduced;
    QThread *_thread;
    QMutex *_mutex;
    bool _is_finished;

public:
    SerializerWriter();

    SerializerWriter(QWaitCondition *cond, QSemaphore *freeSlots, QSemaphore *itcProduced, QThread *thread, QMutex *mutex);

    // void set_values(QWaitCondition* cond, QSemaphore* free, QSemaphore* itc,QThread* thread);

    QWaitCondition *get_cond();

    QSemaphore *get_sem_free();

    QSemaphore *get_sem_produced();

    QThread *get_thread();

public Q_SLOTS:

    void dump_on_disk(IntervalOfContainer *itc, char *filename);

    void load(IntervalOfContainer *itc, char *filename);

    bool is_finished();

    void finish_build();
};

#endif
