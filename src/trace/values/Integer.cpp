/**
 *
 * @file src/trace/values/Integer.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Thibault Soucarre
 * @author Olivier Lagrasse
 * @author Augustin Degomme
 *
 * @date 2024-07-17
 */

#include <string>
#include <sstream>
#include <cstdio>
#include <iomanip> // For std::setprecision
/* -- */
#include "trace/values/Value.hpp"
#include "trace/values/Integer.hpp"

#if defined WIN32 && !defined(__MINGW32__)
#define sscanf sscanf_s
#endif

Integer::Integer() {
    _is_correct = true;
}

Integer::Integer(int n) :
    _value(n) {
    _is_correct = true;
}

Integer::Integer(const std::string &in) {
    int n;
    if (sscanf(in.c_str(), "%d", &n) != 1) {
        _is_correct = false;
    }
    else {
        _value = n;
    }
}

std::string Integer::to_string() const {
    std::ostringstream oss;
    oss << std::setprecision(Value::_PRECISION) << _value;
    return oss.str();
}
