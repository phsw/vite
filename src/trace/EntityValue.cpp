/**
 *
 * @file src/trace/EntityValue.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Arthur Redondy
 * @author Mathieu Faverge
 * @author Thibault Soucarre
 * @author Kevin Coulomb
 * @author Augustin Degomme
 * @author Clement Vuchener
 *
 * @date 2024-07-17
 */
#include <string>
#include "stdio.h"
#include <list>
#include <iostream>
#include <map>
/* -- */
#include "trace/values/Values.hpp"
#include "trace/ContainerType.hpp"
#include "trace/EntityType.hpp"
#include "trace/EntityValue.hpp"
#include "common/Session.hpp"
#include "common/Palette.hpp"
/* -- */
using namespace std;

EntityValue::EntityValue(const Name &name, EntityType *type, map<std::string, Value *> opt) :
    _name(name), _type(type), _opt(opt), _visible(true) {
    Palette *sp = nullptr;
    Color *color = nullptr;
    std::string palette_name = "default";

    // Search opt field for color to set the default (random if not provided within the trace file)
    map<std::string, Value *>::iterator it = opt.find(string("Color"));
    if (it != opt.end()) {
        _filecolor = Color((Color *)(it->second));
        opt.erase(it);
    }
    else {
        if (type->get_class() == _EntityClass_State)
            _filecolor = Color();
        else
            _filecolor = Color(1., 1., 1.);
    }

    // Set the used color to the color selected in the palette if existing, otherwise equal to the filecolor.
    _usedcolor = _filecolor;

    switch (type->get_class()) {
    case _EntityClass_State:
        palette_name = "palette";
        break;
    case _EntityClass_Link:
        palette_name = "link_types";
        break;
    case _EntityClass_Event:
        palette_name = "event_types";
        break;
    case _EntityClass_Variable:
        break;
    default:
        std::cerr << "Error unsupported type for Entity value" << std::endl;
        break;
    }

    if (type->get_class() != _EntityClass_Variable) {
        sp = Session::get_palette(palette_name,
                                  Session::get_current_palette(palette_name));
        color = sp->get_color(name.get_name());
        if (color != nullptr) {
            _usedcolor = Color(color);
        }
        if (sp->is_visible(name.get_name())) {
            _visible = true;
        }
        else {
            _visible = false;
        }
    }
}

EntityValue::~EntityValue() {
    _type = nullptr;

    for (auto &it: _opt) {
        delete it.second;
    }
}

Name EntityValue::get_Name() const {
    return _name;
}

std::string
EntityValue::get_name() const {
    return _name.get_name();
}

std::string
EntityValue::get_alias() const {
    return _name.get_alias();
}

const EntityType *
EntityValue::get_type() const {
    return _type;
}

const map<string, Value *> *
EntityValue::get_extra_fields() const {
    return &_opt;
}

const Color *EntityValue::get_used_color() const {
    return &_usedcolor;
}

const Color *EntityValue::get_file_color() const {
    return &_filecolor;
}

bool EntityValue::get_visible() const {
    return _visible;
}

EntityClass_t
EntityValue::get_class() const {
    return _type->get_class();
}

void EntityValue::set_used_color(const Color &c) {

    _usedcolor = Color(c);

    Q_EMIT changedColor(this);
}

void EntityValue::set_visible(bool b) {
    if (_visible != b) {
        _visible = b;
        Q_EMIT changedVisible(this);
    }
}

void EntityValue::reload_file_color() {
    _usedcolor = _filecolor;
}

#include "moc_EntityValue.cpp"
