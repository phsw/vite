/**
 *
 * @file src/trace/EntityType.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Thibault Soucarre
 * @author Kevin Coulomb
 * @author Olivier Lagrasse
 * @author Augustin Degomme
 * @author Luigi Cannarozzo
 * @author Clement Vuchener
 *
 * @date 2024-07-17
 */
#include <string>
#include <list>
#include <map>
/* -- */
#include "trace/values/Values.hpp"
#include "trace/ContainerType.hpp"
#include "trace/EntityValue.hpp"
#include "trace/EntityType.hpp"
/* -- */
using namespace std;

EntityType::EntityType(EntityClass_t ec, Name name,
                       ContainerType *container_type,
                       map<std::string, Value *> &opt) :
    _class(ec),
    _name(std::move(name)), _container_type(container_type) {
    if (opt.empty())
        _extra_fields = nullptr;
    else {
        _extra_fields = new map<std::string, Value *>(opt);
        opt.clear();
    }
}

EntityType::~EntityType() {
    // Destruction of the list _values
    // As long as everything has not been cleaned
    std::map<std::string, EntityValue *>::iterator it = _values.begin();
    std::map<std::string, EntityValue *>::iterator itend = _values.end();
    for (; it != itend; ++it) {
        delete (it->second);
    }
    _values.clear();
    _container_type = nullptr;
    if (_extra_fields != nullptr) {
        map<std::string, Value *>::iterator it = _extra_fields->begin();
        map<std::string, Value *>::iterator itend = _extra_fields->end();

        for (; it != itend; ++it) {
            delete (it->second);
        }
        _extra_fields->clear();
        delete _extra_fields;
    }
}

void EntityType::add_value(EntityValue *value) {
    _values.insert(pair<std::string, EntityValue *>(value->get_alias(),
                                                    value));
}

const Name
EntityType::get_Name() const {
    return _name;
}

const std::string
EntityType::get_name() const {
    return _name.get_name();
}

const std::string
EntityType::get_alias() const {
    return _name.get_alias();
}

EntityClass_t EntityType::get_class() const {
    return _class;
}

const ContainerType *EntityType::get_container_type() const {
    return _container_type;
}

const std::map<std::string, EntityValue *> *EntityType::get_values() const {
    return &_values;
}

void EntityType::set_name(Name name) {
    _name = std::move(name);
}

void EntityType::set_container_type(ContainerType *container_type) {
    _container_type = container_type;
}

void EntityType::set_extra_fields(std::map<std::string, Value *> *extra_fields) {
    _extra_fields = extra_fields;
}

void EntityType::set_values(std::map<std::string, EntityValue *> values) {
    _values = std::move(values);
}

map<std::string, Value *> *EntityType::get_extra_fields() const {
    return _extra_fields;
}
