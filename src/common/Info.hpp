/**
 *
 * @file src/common/Info.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Thibault Soucarre
 * @author Jule Marcoueille
 * @author Olivier Lagrasse
 * @author Augustin Degomme
 *
 * @date 2024-07-17
 */
/*!
 *\file Info.hpp
 */

#ifndef INFO_HPP
#define INFO_HPP
#include <string>
/*!
 * \brief Class used to store information.
 *
 * Info class uses static attributes to share information between ViTE modules. This class cannot be instantiate thus its constructor has a private scope.
 */
class Info
{

private:
    /*!
     * \brief The class constructor.
     */
    Info();

public:
    /*!
     * \brief The class destructor.
     */
    virtual ~Info();

    /*!
     * \brief Release all Info class attributes.
     */
    static void release_all();

    /*!
     * \brief Sub structure to store screen information.
     */
    struct Screen
    {

        /*!
         * \brief The screen width. (in pixel)
         */
        static unsigned int width;

        /*!
         * \brief The screen height. (in pixel)
         */
        static unsigned int height;
    };

    /*!
     * \brief Sub structure to store container information.
     */
    struct Container
    {

        /*!
         * \brief Minimum x position.
         */
        static Element_pos x_min;

        /*!
         * \brief Maximum x position.
         */
        static Element_pos x_max;

        /*!
         * \brief Minimum y position.
         */
        static Element_pos y_min;

        /*!
         * \brief Maximum y position.
         */
        static Element_pos y_max;
    };

    /*!
     * \brief Sub structure to store state information.
     */
    struct Entity
    {

        /*!
         * \brief Minimum x position.
         */
        static Element_pos x_min;

        /*!
         * \brief Maximum x position.
         */
        static Element_pos x_max;

        /*!
         * \brief Minimum y position.
         */
        static Element_pos y_min;

        /*!
         * \brief Maximum y position.
         */
        static Element_pos y_max;
    };

    /*!
     * \brief Sub structure to store render area information.
     */
    struct Render
    {

        /*!
         * \brief The render width. (no unit)
         */
        static Element_pos width;

        /*!
         * \brief The render height. (no unit)
         */
        static Element_pos height;

        /*!
         * \brief Contains the state of tke ALT key.
         * true -> key pushed.
         * false -> key released.
         */
        static bool _key_alt;

        /*!
         * \brief Contains the state of tke CTRL key.
         * true -> key pushed.
         * false -> key released.
         */
        static bool _key_ctrl;

        /*!
         * \brief Specify the shape of arrows heads. 0 is triangle, 1 is a point and 2 is none.
         */
        static int _arrows_shape;

        /*!
         * \brief Prevent arrows to be displayed (used with Display List since it should highly decreased performance).
         */
        static bool _no_arrows;

        /*!
         * \brief Prevent events to be displayed (used with Display List since it should highly decreased performance).
         */
        static bool _no_events;

        /*!
         * \brief Control if the color of State is uniform (value is false) or shaded (value is true). (By default, shaded)
         */
        static bool _shaded_states;

        /*!
         * \brief Control if the vertical line is on. (By default, yes)
         */
        static bool _vertical_line;

        /*!
         * \brief Contains the trace minimum visible time.
         */
        static Element_pos _x_min_visible;

        /*!
         * \brief Contains the trace maximum visible time.
         */
        static Element_pos _x_max;

        /*!
         * \brief Contains the trace minimum visible time, uncorrected (state width isn't removed)
         */
        static Element_pos _x_min;

        /*!
         * \brief Contains the trace maximum visible time.
         */
        static Element_pos _x_max_visible;

        /*!
         * \brief Contains the x position of selection.
         */
        static Element_pos _info_x;

        /*!
         * \brief Contains the y position of selection.
         */
        static Element_pos _info_y;

        /*!
         * \brief Contains the accurate position of selection.
         */
        static Element_pos _info_accurate;

        /*!
         * \brief To show the x beginning of the trace.
         */
        static const int X_TRACE_BEGINNING = 0;

        /*!
         * \brief To show the y beginning of the trace.
         */
        static const int Y_TRACE_BEGINNING = 1;

        /*!
         * \brief To show the x ending of the trace.
         */
        static const int X_TRACE_ENDING = 2;

        /*!
         * \brief To show the y ending of the trace.
         */
        static const int Y_TRACE_ENDING = 3;

        /*!
         * \brief To show the horizontal entire trace.
         */
        static const int X_TRACE_ENTIRE = 4;

        /*!
         * \brief To show the vertical entire trace.
         */
        static const int Y_TRACE_ENTIRE = 5;

        /*!
         * \brief To show the left screen of the current view.
         */
        static const int X_SCREEN_LEFT_MOVE = 6;

        /*!
         * \brief To show the right screen of the current view.
         */
        static const int X_SCREEN_RIGHT_MOVE = 7;
    };

    /*!
     * \brief Sub structure to store trace information.
     */
    struct Trace
    {

        /*!
         * \brief The depth (number of stacked containers) of the data structure.
         */
        static int depth;
    };

    struct Splitter
    {
        /*!
         * \brief are we splitting ?
         */
        static bool split;
        /*!
         * \brief are we loading from a splitted set of files (needed in core to know which action to initiate)
         */
        static bool load_splitted;
        /*!
         * \brief are we loadinga preview version of a splitted file (needed in core to know which action to initiate)
         */
        static bool preview;
        /*!
         * \brief path of the folder to split
         */
        static std::string path;
        /*!
         * \brief filename of the splitted file, used to name the folder
         */
        static std::string filename;
        /*!
         * \brief filename of the node selection file, used to select nodes to display in PArserSplitted
         */
        static std::string xml_filename;
        /*!
         * \brief min time we want to load
         */
        static Element_pos _x_min;
        /*!
         * \brief max time we want to load
         */
        static Element_pos _x_max;
    };
};

#endif
