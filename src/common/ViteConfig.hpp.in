/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
*/
/*!
 *   @file vite_config.hpp
 *   @brief This file defines all set of macro defined in cmake
 *   @author Mathieu Faverge
 *
 */

#ifndef _VITE_CONFIG_H_
#define _VITE_CONFIG_H_

#define VITE_VERSION_MAJOR @VITE_VERSION_MAJOR@
#define VITE_VERSION_MINOR @VITE_VERSION_MINOR@
#define VITE_VERSION_PATCH @VITE_VERSION_PATCH@

#define VITE_VERSION "@VITE_VERSION@"
#define VITE_DATE    @VITE_DATE@

#cmakedefine USE_QT6
#cmakedefine HAVE_QT5_15

/* Optimization options */
#cmakedefine VITE_ENABLE_SERIALIZATION
#cmakedefine VITE_ENABLE_VBO
#cmakedefine VITE_ENABLE_MT_PARSERS
#cmakedefine USE_VULKAN

/* Debug options */
#cmakedefine VITE_DBG_MEMORY_USAGE
#cmakedefine VITE_DBG_MEMORY_TRACE

/* Trace format options */
#cmakedefine VITE_ENABLE_TAU

/* Long command line options */
#cmakedefine HAVE_GETOPT_LONG

#endif /* _VITE_CONFIG_H_ */
