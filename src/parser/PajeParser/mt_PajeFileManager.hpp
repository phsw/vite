/**
 *
 * @file src/parser/PajeParser/mt_PajeFileManager.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Augustin Degomme
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */

/**
 * \file    mt_PajeFileManager.hpp
 *  Visual Trace Explorer
 *
 *  Release Date: January, 2nd 2010
 *  ViTE is a software to vivisualize execution trace provided by
 *  a group of student from ENSEIRB and INRIA Bordeaux - Sud-Ouest
 *
 * @version 1.1
 * @author  Kevin Coulomb
 * @author  Mathieu Faverge
 * @author  Johnny Jazeix
 * @author  Olivier Lagrasse
 * @author  Jule Marcoueille
 * @author  Pascal Noisette
 * @author  Arthur Redondy
 * @author  Clément Vuchener
 * @date    2010-01-02
 *
 **/

#ifndef FILE_HPP
#define FILE_HPP

#include <string.h>
#ifdef WIN32
#include <windows.h>
#else
#include <unistd.h>
#include <sys/mman.h>
#endif
#include <stdio.h>
#include <fcntl.h>
#define _PAJE_NBMAXTKS 32

#include <cstring>
#include <string>
#include <fstream>
#include <iostream>
#include <QMetaType>
using namespace std;
/**
 * \struct PajeLine
 *
 * Brief structure to store information read
 * on each line
 *
 * \param _id Line number
 * \param _nbtks Number of tokens found on the line
 * \param _tokens Pointers on the found tokens
 *
 */
typedef struct PajeLine
{
    int _id;
    int _nbtks;
    char **_tokens;

} PajeLine_t;
// Q_DECLARE_METATYPE(PajeLine);

/**
 *  \class mt_PajeFileManager
 *
 *  File manager to read files using Paje syntax.
 *  Each line is read one after one and stored in
 *  the PajeLine structure associated to the class.
 *
 * \sa Parser
 * \sa ParserPaje
 * \sa ParserVite
 */
class mt_PajeFileManager : public std::ifstream
{

private:
    std::string _filename;
    unsigned long long _filesize;
    int _total_filesize;
    int _nbchunks;
    int _current_chunk;
    int _mapped_size;
    bool _eof;
    bool _eoc;
    unsigned int _lineid;
    int _nbtks;
    char **_tokens;
    char *_line;
    char *_old_line;

#ifdef WIN32
    HANDLE _fd;
    HANDLE _fm;

#else
    FILE *_fd;
#endif
    unsigned long long _pageSize;
    unsigned long long _offset;
    unsigned long long _global_offset;

    mt_PajeFileManager(const mt_PajeFileManager &);

public:
    /*!
     *  \brief Constructor for the file
     */
    mt_PajeFileManager();

    /*!
     *  \brief  Constructor for the file
     *  \param  filename : a filename
     */
    mt_PajeFileManager(const char *filename);

    /*!
     *  \brief Destructor
     *  Destroy the file
     */
    ~mt_PajeFileManager() override;

    /*!
     *  \fn open(const char * filename, ios_base::openmode mode)
     *  \brief Open the file
     *  \param filename the file name
     *  \param mode the opening mode
     */
    void open(const char *filename);

    /*!
     *  \fn close()
     *  \brief Close the file if already opened
     */
    void close();

    /*!
     *  \fn eof()
     *  \brief check the end of file
     */
    bool eof();

    /*!
     *  \fn eoc()
     *  \brief check the end of the opened chunk of file
     */
    bool eoc();

    /*!
     *  \fn close_old_chunk()
     *  \brief release the previous chunk of file
     */
    void close_old_chunk();

    /*!
     *  \fn get_filesize() const;
     *  \return The length of the file opened
     */
    int get_filesize() const;

    /*!
     *  \fn get_size_loaded();
     *  \return The size already loaded
     */
    int get_size_loaded();

    /*!
     *  \fn get_percent_loaded()
     *  \return The percent of the file loaded (between 0 and 1).
     */
    float get_percent_loaded();

    /*!
     *  \fn get_line(PajeLine *lineptr)
     *  \brief Get the next line.
     *  \param lineptr the line filled in
     *  \return the next line
     */
    int get_line(PajeLine *lineptr);

    /*!
     *  \fn print_line()
     *  \brief Print the current line for debug
     */
    void print_line();
};

#endif // FILE_HPP
