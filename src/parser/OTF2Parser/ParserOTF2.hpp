/**
 *
 * @file src/parser/OTF2Parser/ParserOTF2.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Francois Trahay
 * @author Johnny Jazeix
 *
 * @date 2024-07-17
 */
/**
 *  @file ParserOTF2.hpp
 *
 *  @brief the implementation of Parser for OTF2 traces.
 *
 *  @author Lagrasse Olivier
 *  @author Johnny Jazeix
 *  @author Mathieu Faverge
 *
 */
#ifndef PARSEROTF2_HPP
#define PARSEROTF2_HPP

/*!
 *
 * \class ParserOTF2
 * \brief parse the input data format of OTF2.
 *
 */
class ParserOTF2 : public Parser
{
private:
    ParserOTF2(const ParserOTF2 &);

public:
    /*!
     *  \fn ParserOTF2()
     */
    ParserOTF2();
    ParserOTF2(const std::string &filename);
    ~ParserOTF2();

    /*!
     *  \fn parse(Trace &trace, bool finish_trace_after_parse = true)
     *  \param trace : the structure of data to fill
     *  \param finish_trace_after_parse boolean set if we do not have to finish the trace after parsing
     */
    void parse(Trace &trace,
               bool finish_trace_after_parse = true);

    /*!
     *  \fn get_percent_loaded() const
     *  \brief return the size of the file already read.
     *  \return the scale of the size already loaded of the file by the parser. (between 0 and 1)
     */
    float get_percent_loaded() const;
};

#endif // PARSEROTF2_HPP
