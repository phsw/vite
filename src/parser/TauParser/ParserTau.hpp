/**
 *
 * @file src/parser/TauParser/ParserTau.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
/*!
 * \file ParserTau.hpp
 * \brief the implementation of Parser for TAU traces.
 */

#ifndef PARSERTAU_HPP
#define PARSERTAU_HPP

#include "parser/TauParser/TauStructs.hpp"

/*!
 *
 * \class ParserTau
 * \brief parse the input data format of Tau.
 *
 */
class ParserTau : public Parser
{
private:
    Ttf_CallbacksT _callbacks;
    Ttf_FileHandleT _file;

    /* The string is corresponding to : node_id+'-'+thread_id */
    static std::map<std::string, Tau::Container> _containers;
    static std::map<int, Tau::State> _states;
    static std::map<int, Tau::StateGroup> _states_groups;
    static std::map<int, Tau::Event> _events;

    static double _clock_period;

    /*!
     * Table containing default colors.
     * We take the state id modulo the max number to get the color.
     */
    static std::vector<Color *> _default_colors;

    /*!
     *  \fn static int set_timer_resolution(void *trace, double clkPeriod)
     *  \brief Callback method to set the timer resolution
     *  \param trace : useless here
     *  \param clkPeriod : the timer resolution
     *  \return 0, always.
     */
    static int set_timer_resolution(void *trace, double clkPeriod);

    /*!
     *  \fn static int def_process(void *trace, unsigned int nodeToken, unsigned int threadToken, const char *threadName)
     *  \brief Callback method to define a container
     *  \param trace : useless here
     *  \param nodeToken : the node id of the container
     *  \param threadToken : the thread id of the container
     *  \param threadName : the name of the container
     *  \return 0, always.
     */
    static int def_process(void *trace, unsigned int nodeToken, unsigned int threadToken, const char *threadName);

    /*!
     *  \fn static int def_state(void *trace, unsigned int stateToken, const char *stateName, unsigned int stateGroupToken)
     *  \brief Callback method to define a state
     *  \param trace : useless here
     *  \param stateToken : the state id
     *  \param stateName : the state name
     *  \param stateGroupToken : the id of the state group
     *  \return 0, always.
     */
    static int def_state(void *trace, unsigned int stateToken, const char *stateName, unsigned int stateGroupToken);

    /*!
     *  \fn static int def_state_group(void *trace, unsigned int stateGroupToken, const char *stateGroupName)
     *  \brief Callback method to define a state group (StateType)
     *  \param trace : useless here
     *  \param stateGroupToken : the state group id
     *  \param stateGroupName : the state group name
     *  \return 0, always.
     */
    static int def_state_group(void *trace, unsigned int stateGroupToken, const char *stateGroupName);

    /*!
     *  \fn static int def_event(void *trace, unsigned int userEventToken, const char *userEventName, int monotonicallyIncreasing)
     *  \brief Callback method to define a event
     *  \param trace : useless here
     *  \param userEventToken : the event id
     *  \param userEventName : the event name
     *  \param monotonicallyIncreasing : unused.
     *  \return 0, always.
     */
    static int def_event(void *trace, unsigned int userEventToken, const char *userEventName, int monotonicallyIncreasing);

    /*!
     *  \fn static int enter_state(void *trace, double time, unsigned int nid, unsigned int tid, unsigned int stateid)
     *  \brief Callback method to start a new state
     *  \param trace : the trace where we add the state.
     *  \param time : the time when entering in the state
     *  \param nid : the node id of the container
     *  \param tid : the thread id of the container
     *  \param stateid : the state id
     *  \return 0, always.
     */
    static int enter_state(void *trace, double time, unsigned int nid, unsigned int tid, unsigned int stateid);

    /*!
     *  \fn static int leave_state(void *trace, double time, unsigned int nid, unsigned int tid, unsigned int stateid)
     *  \brief Callback method to leave a state
     *  \param trace : the trace where we add the state.
     *  \param time : the time when entering in the state
     *  \param nid : the node id of the container
     *  \param tid : the thread id of the container
     *  \param stateid : the state id
     *  \return 0, always.
     */
    static int leave_state(void *trace, double time, unsigned int nid, unsigned int tid, unsigned int stateid);

    /*!
     *  \fn static int event_triggered(void *trace, double time, unsigned int nid, unsigned int tid, unsigned int eventid, long long eventValue)
     *  \brief Callback method to create a event
     *  \param trace : the trace where we add the state.
     *  \param time : the time when entering in the state
     *  \param nid : the node id of the container
     *  \param tid : the thread id of the container
     *  \param eventid : the event id
     *  \param eventValue : the event value
     *  \return 0, always.
     */
    static int event_triggered(void *trace, double time, unsigned int nid, unsigned int tid, unsigned int eventid, long long eventValue);

    /*!
     *  \fn static int send_message (void *trace, double time, unsigned int srcNid, unsigned int srcTid, unsigned int destNid, unsigned int destTid, unsigned int messageSize, unsigned int messageTag, unsigned int messageComm)
     *  \brief Callback method to send a message
     *  \param trace : the trace where we add the state.
     *  \param time : the time when entering in the state
     *  \param srcNid : the node id of the container source
     *  \param srcTid : the thread id of the container source
     *  \param destNid : the node id of the container destination
     *  \param destTid : the thread id of the container destination
     *  \param messageSize : unused (could be added as optional field)
     *  \param messageTag : used as key for finding the end of the message.
     *  \param messageComm : used as the value of the message.
     *  \return 0, always.
     */
    static int send_message(void *trace, double time, unsigned int srcNid, unsigned int srcTid, unsigned int destNid, unsigned int destTid, unsigned int messageSize, unsigned int messageTag, unsigned int messageComm);

    /*!
     *  \fn static int receive_message (void *trace, double time, unsigned int srcNid, unsigned int srcTid, unsigned int destNid, unsigned int destTid, unsigned int messageSize, unsigned int messageTag, unsigned int messageComm)
     *  \brief Callback method to receive a message
     *  \param trace : the trace where we add the state.
     *  \param time : the time when entering in the state
     *  \param srcNid : the node id of the container source
     *  \param srcTid : the thread id of the container source
     *  \param destNid : the node id of the container destination
     *  \param destTid : the thread id of the container destination
     *  \param messageSize : unused (could be added as optional field)
     *  \param messageTag : used as key for finding the begin of the message.
     *  \param messageComm : used as the value of the message.
     *  \return 0, always.
     */
    static int receive_message(void *trace, double time, unsigned int srcNid, unsigned int srcTid, unsigned int destNid, unsigned int destTid, unsigned int messageSize, unsigned int messageTag, unsigned int messageComm);

    /*!
     *  \fn static int end_trace(void *trace, unsigned int nodeToken, unsigned int threadToken)
     *  \brief Callback method to destroy a container
     *  \param trace : the trace where we destroy the container.
     *  \param nid : the node id of the container
     *  \param tid : the thread id of the container
     *  \return 0, always.
     */
    static int end_trace(void *trace, unsigned int nid, unsigned int tid);

    ParserTau(const ParserTau &);

public:
    /*!
     *  \fn ParserTau()
     */
    ParserTau();
    ParserTau(const std::string &filename);
    ~ParserTau() override;

    /*!
     *  \fn parse(Trace &trace, bool finish_trace_after_parse)
     *  \param trace : the structure of data to fill
     *  \param finish_trace_after_parse boolean set if we do not have to finish the trace after parsing
     */
    void parse(Trace &trace, bool finish_trace_after_parse = true) override;

    /*!
     *  \fn get_percent_loaded() const
     *  \brief return the size of the file already read.
     *  \return the scale of the size already loaded of the file by the parser. (between 0 and 1)
     */
    float get_percent_loaded() const override;

    static Color *get_color(int func_id);
};

#endif // PARSERTAU_HPP
