/**
 *
 * @file src/parser/Parser.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Olivier Lagrasse
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */

#include <QEventLoop>
/* -- */
#include <string>
#include <list>
#include <map>
/* -- */
#include "trace/values/Values.hpp"
#include "trace/EntityTypes.hpp"
#include "trace/Entitys.hpp"
#include "trace/Trace.hpp"
/* -- */
#include "parser/Parser.hpp"
using namespace std;

Parser::Parser() :
    _is_finished(false), _is_canceled(false), _parsing_event_loop(nullptr) { }

Parser::Parser(const string &filename) :
    _is_finished(false), _is_canceled(false), _parsing_event_loop(nullptr) {
    set_file_to_parse(filename);
}

void Parser::parse(const std::string &filename,
                   Trace &trace,
                   bool finish_trace_after_parse) {
    set_file_to_parse(filename);
    parse(trace, finish_trace_after_parse);
}

void Parser::set_file_to_parse(const string &filename) {
    _q.push(filename);
}

const string Parser::get_next_file_to_parse() {
    if (_q.size() > 0) {
        const string retval = _q.front();
        _q.pop();
        return retval;
    }
    return "";
}

bool Parser::is_end_of_parsing() const {
    return _is_finished;
}

void Parser::finish() {
    _is_finished = true;
    // Active wait, otherwise the stop_parsing_event_loop could be sent before the event loop started
    while (!_parsing_event_loop->isRunning()) { }
    Q_EMIT stop_parsing_event_loop();
}

void Parser::set_canceled() {
    _is_canceled = true;
}

bool Parser::is_canceled() const {
    return _is_canceled;
}

void Parser::set_event_loop(QEventLoop *event_loop) {
    _parsing_event_loop = event_loop;
}

#include "moc_Parser.cpp"
