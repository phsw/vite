#!/usr/bin/env bash
###
#
#  @file release.sh
#  @copyright 2013-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
#                       Univ. Bordeaux. All rights reserved.
#
#  @brief Script to generate the release when pushing a tag
#
#  @version 1.4.0
#  @author Florent Pruvost
#  @author Mathieu Faverge
#  @author Camille Ordronneau
#  @date 2024-07-23
#
###

#
# Steps to do the release:
#    - Update information in the code
#    - Update the ChangeLog
#    - Create a tag named vx.x.x and push it on solverstack (will trigger the CI to generate the release)
#
changelog=""
function gen_changelog()
{
    local firstline=$( grep -n "^ViTE " ChangeLog | head -n 1 | cut -d ':' -f 1 )
    firstline=$(( firstline + 2 ))
    echo $firstline
    local lastline=$( grep -n "^ViTE " ChangeLog | head -n 2 | tail -n 1 | cut -d ':' -f 1 )
    lastline=$(( lastline - 1 ))
    echo $lastline

    changelog="Changes:\n"
    for i in `seq $firstline $lastline`
    do
        local line=$( head -n $i ChangeLog | tail -n 1 )
        changelog="$changelog$line\n"
        echo $line
    done
}

release=""
function get_release()
{
    local firstline=$( grep -n "^ViTE " ChangeLog | head -n 1 | cut -d ':' -f 1 )
    release=$( head -n 1 ChangeLog | sed -E 's/ViTE ([0-9]+(\.[0-9]+){1,2}) .+/\1/' )
}

set -x

# Get the release name through the branch name, and through the ChangeLog file.
# Both have to match to be correct
RELEASE_NAME=`echo $CI_COMMIT_TAG | cut -d v -f 2`
get_release

if [ -z "$RELEASE_NAME" -o -z "$release" -o "$RELEASE_NAME" != "$release" ]
then
    echo "Commit name $RELEASE_NAME is different from ChangeLog name $release"
    exit 1
fi

readonly FULL_RELEASE_NAME=vite-$RELEASE_NAME
readonly GITLAB_API_ENDPOINT=https://gitlab.inria.fr/api/v4/projects/$CI_PROJECT_ID

# generate the archive without the web folder
git archive --format=tar HEAD --prefix=$FULL_RELEASE_NAME/ -o $FULL_RELEASE_NAME.tar
tar -f $FULL_RELEASE_NAME.tar --delete $FULL_RELEASE_NAME/web
gzip $FULL_RELEASE_NAME.tar

# upload the source archive to the Gitlab's Package registry
curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file ./$FULL_RELEASE_NAME.tar.gz "$GITLAB_API_ENDPOINT/packages/generic/source/$CI_COMMIT_TAG/$FULL_RELEASE_NAME.tar.gz"

# extract the change log from ChangeLog
gen_changelog
echo -e $changelog
escaped_changelog=$(echo $changelog | sed 's/\"/\\\"/g')

# Try to remove the release if it already exists
curl --request DELETE --header "JOB-TOKEN: $CI_JOB_TOKEN" $GITLAB_API_ENDPOINT/releases/v$RELEASE_NAME

# create the release associated to the tag
curl --header "Content-Type: application/json" --header "JOB-TOKEN: $CI_JOB_TOKEN" --request POST $GITLAB_API_ENDPOINT/releases --data @- <<EOF
{
    "name": "v$RELEASE_NAME",
    "tag_name": "v$RELEASE_NAME",
    "ref": "$CI_COMMIT_REF_NAME",
    "description": "$escaped_changelog",
    "assets": {
        "links": [{
            "name": "Download release $FULL_RELEASE_NAME.tar.gz",
            "url": "$GITLAB_API_ENDPOINT/packages/generic/source/$CI_COMMIT_TAG/$FULL_RELEASE_NAME.tar.gz"
        }]
    }
}
EOF
