/**
 *
 * @file tests/stubs/Date.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#include "Date.hpp"

Date::Date(){
    me.erase();
}
std::string Date::to_string() const{
    return me;
}

bool Date::instantiate(const std::string &in, Date &out){
    std::cout << "instantiate " << in << " in date" << std::endl;
    return true;
}
