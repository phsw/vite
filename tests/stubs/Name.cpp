/**
 *
 * @file tests/stubs/Name.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#include "Name.hpp"

Name::Name() {
    me.erase();
}

std::string Name::to_string() const {
    return me;
}

void Name::set_alias(const std::string &alias) {
    me = alias;
}

void Name::set_name(const std::string &name) {
    me = name;
}

String Name::to_String() {
    return String(me);
}
