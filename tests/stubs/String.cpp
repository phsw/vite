/**
 *
 * @file tests/stubs/String.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#include "String.hpp"

String::String() {
    me.erase();
}

String::String(const std::string &s) :
    me(s) { }

std::string String::to_string() const {
    return me;
}

String String::operator=(const std::string &s) {
    me = s;
    return *this;
}
