/**
 *
 * @file tests/generator/OTFGenerator.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#include "OTFGenerator.hpp"

OTFGenerator::OTFGenerator   (){
}

OTFGenerator::~OTFGenerator  (){
}

void OTFGenerator::initTrace (QString name, int depth, int procNbr, int stateNbr, int eventNbr, int linkTypeNbr, int varNbr){
}

void OTFGenerator::addState  (int proc, int state, double time){
}

void OTFGenerator::startLink (int proc, int type , double time){
}

void OTFGenerator::endLink   (int proc, int type , double time){
}

void OTFGenerator::addEvent  (int proc, int type , double time){
}

void OTFGenerator::incCpt    (int proc, int var  , double time){
}

void OTFGenerator::decCpt    (int proc, int var  , double time){
}

void OTFGenerator::endTrace  () {
}


