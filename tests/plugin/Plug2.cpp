/**
 *
 * @file tests/plugin/Plug2.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#include "Plugin.hpp"

class Plug2 : public Plugin {
public: 
    void init() { printf("Plug2 : init\n"); }
    void set_arguments(std::map<std::string /*argname*/, QVariant */*argValue*/>) {};

    std::string get_name() { return "Plug2"; }

public Q_SLOTS:
    void execute() { printf("Plug2 : exec\n"); }
};

extern "C" {
    Plugin *create() { return new Plug2(); }
}
