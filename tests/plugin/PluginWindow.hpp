/**
 *
 * @file tests/plugin/PluginWindow.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#ifndef PLUGINWINDOW_HPP
#define PLUGINWINDOW_HPP

#include <QObject>
#include <QWidget>
#include <QVBoxLayout>
#include <QTabWidget>
#include <QPushButton>

class Plugin;

class PluginWindow : public QWidget {
    Q_OBJECT
private:
    QVBoxLayout *_layout;
    QTabWidget *_tab_widget;
    QPushButton *_execute_button;
    std::vector<Plugin *> _plugins;
    int _current_index;

public:
    PluginWindow();
    ~PluginWindow();
    void load_list();
    void load_plugin(const std::string &plugin_name);

private:
    void load_shared_plugins();
    void add_plugin(Plugin *plug);

public Q_SLOTS:
    void init_plugin(int index);
    void execute_plugin();
};


#endif // PLUGINWINDOW_HPP
