/**
 *
 * @file tests/plugin/Plug3.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#ifndef PLUG3_HPP
#define PLUG3_HPP

#include "Plugin.hpp"

class Plug3 : public Plugin {
public: 
    void init() { printf("Plug3 : init\n"); }
    void set_arguments(std::map<std::string /*argname*/, QVariant */*argValue*/>) {};

    std::string get_name() { return "Plug3"; }

public Q_SLOTS:
    void execute() { printf("Plug3 : exec\n"); }
};

#endif // PLUG3_HPP
