/**
 *
 * @file plugins/TraceInfos/TraceInfos.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#ifndef TRACE_INFOS_HPP
#define TRACE_INFOS_HPP

class Trace_infos : public Plugin
{
private:
    QTextEdit *_text_info;

    void set_container_infos(QString &text) const;

    void set_states_infos(QString &text) const;
    void get_states_name_rec(Node<StateChange> *parent, std::set<std::string> &state_list) const;
    void set_events_infos(QString &text) const;

    void set_variables_infos(QString &text) const;

    void set_links_infos(QString &text) const;

public:
    Trace_infos();
    ~Trace_infos();

    void init();
    void clear();

    void set_arguments(std::map<std::string /*argname*/, QVariant * /*argValue*/>);
    std::string get_name();

public Q_SLOTS:
    void execute();
};

extern "C"
#ifdef WIN32
    __declspec(dllexport) // no comment
#endif
    Plugin *create() {
    return new Trace_infos();
}

#endif // TRACE_INFOS_HPP
