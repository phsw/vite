/**
 *
 * @file plugins/MatrixVisualizer/Common/Zooming.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Arthur Chevalier
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Hamza Benmendil
 *
 * @date 2024-07-17
 */
#include <assert.h>

#include "Zooming.hpp"

#include "../Helper.hpp"

Zooming::Zooming(symbol_matrix_t *matrix) :
    Zoom(matrix) {
    // Fill correct colors
    move(0.f, 1.f, 0.f, 1.f);
}

Zooming::~Zooming() { }

void Zooming::move(double x_start, double x_end, double y_start, double y_end) {
    symbol_cblk_t *cblk;
    symbol_blok_t *blok;
    int i, j, m, n;

    // Check positions
    assert(x_end >= x_start);
    assert(y_end >= y_start);
    assert(y_end >= x_start);

    // Check for out of bounds
    assert(x_start >= 0.);
    assert(y_start >= 0.);
    assert(x_end <= 1.);
    assert(y_end <= 1.);

    // Convert to column/row indexes
    int start_col = x_start * m_matrix->m_colsnbr;
    int end_col = x_end * m_matrix->m_colsnbr - 1;
    int start_row = y_start * m_matrix->m_rowsnbr;
    int end_row = y_end * m_matrix->m_rowsnbr - 1;

    // Find first cblk
    int start_cblk = 0;
    int end_cblk = 0;

    // Search for beginnings
    cblk = m_matrix->m_cblktab;
    for (i = 0; i < m_matrix->m_cblknbr; ++i, cblk++) {
        if ((cblk->m_fcolnum <= start_col) && (start_col <= cblk->m_lcolnum)) {
            start_cblk = i;
            start_col = cblk->m_fcolnum;
        }
        if ((cblk->m_fcolnum <= start_row) && (start_row <= cblk->m_lcolnum)) {
            start_row = cblk->m_fcolnum;
        }
        if ((cblk->m_fcolnum <= end_col) && (end_col <= cblk->m_lcolnum)) {
            end_cblk = i + 1;
            end_col = cblk->m_lcolnum;
        }
        if ((cblk->m_fcolnum <= end_row) && (end_row <= cblk->m_lcolnum)) {
            end_row = cblk->m_lcolnum;
        }
    }

    int nb_cols = end_col - start_col + 1;
    int nb_rows = end_row - start_row + 1;

    for (i = 0; i < DEFAULT_LEVEL; ++i) {
        for (j = 0; j < DEFAULT_LEVEL; ++j) {
            m_colors[i][j] = 1.f;
        }
    }

    float x_coeff = (float)DEFAULT_LEVEL / ((float)nb_cols);
    float y_coeff = (float)DEFAULT_LEVEL / ((float)nb_rows);

    cblk = m_matrix->m_cblktab + start_cblk;
    for (i = start_cblk; i < end_cblk; ++i, cblk++) {
        int fbloknum = cblk[0].m_bloknum;
        int lbloknum = cblk[1].m_bloknum;

        // Get first block size in col from x to x_end
        int x = (cblk->m_fcolnum - start_col) * x_coeff;
        int x_end = (cblk->m_lcolnum + 1 - start_col) * x_coeff;

        float cblk_color = cblk->m_color;

        blok = m_matrix->m_bloktab + fbloknum;
        for (j = fbloknum; j < lbloknum; ++j, blok++) {
            if ((blok->m_lrownum < start_row) || (blok->m_frownum > end_row)) {
                continue;
            }

            // Get first block size in row from y to y_end
            int y = (blok->m_frownum - start_row) * y_coeff;
            int y_end = (blok->m_lrownum + 1 - start_row) * y_coeff;

            float color = blok->m_color == -1. ? cblk_color : blok->m_color;

            for (m = x; m < x_end; m++) {
                for (n = y; n < y_end; n++) {
                    m_colors[m][n] = color;
                }
            }
        }
    }
}
