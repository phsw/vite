/**
 *
 * @file plugins/MatrixVisualizer/Common/Zoom.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Hamza Benmendil
 * @author Johnny Jazeix
 * @author Camille Ordronneau
 *
 * @date 2024-07-17
 */
#include "Zoom.hpp"

Zoom::Zoom(symbol_matrix_t *matrix) :
    m_matrix(matrix) { }

Zoom::~Zoom() { }

GLfloat Zoom::getColor(int x, int y) const {
    return m_colors[x][y];
}