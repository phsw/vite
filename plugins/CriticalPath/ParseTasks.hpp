/**
 *
 * @file plugins/CriticalPath/ParseTasks.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#ifndef PARSETASK_HPP
#define PARSETASK_HPP

#include <iostream>
#include <fstream>
#include <string.h>
#include <vector>

/*!
 * \brief Parses the .rec file Returns -1 if errors
 * \param filename The .rec file to parse
 * \param t Vector to complete with the time of each task
 */
int parse_task(std::string &filename, std::vector<double> &t);

/*!
 * \brief Returns the time of task
 * \param JobID ID of the task
 * \param t Vector that contains time of tasks
 */
double get_time(int jobId, std::vector<double> &t);

#endif
