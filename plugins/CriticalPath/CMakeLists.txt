###
#
#  This file is part of the ViTE project.
#
#  This software is governed by the CeCILL-A license under French law
#  and abiding by the rules of distribution of free software. You can
#  use, modify and/or redistribute the software under the terms of the
#  CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
#  URL: "http://www.cecill.info".
#
#    @version 1.2.0
#    @authors BOULLIT Mohamed Fayçal
#    @authors ELOMARI ALOUAI Ismail
#
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)

set(CRITICALPATH_HDRS
    CriticalPath.hpp

    ParseDAG.hpp
    ParseTasks.hpp
    ParserStats.hpp
    DrawStats.hpp
)

set(CRITICALPATH_SRCS
    CriticalPath.cpp

    ParseDAG.cpp
    ParseTasks.cpp
    ParserStats.cpp
    DrawStats.cpp
)

set(CRITICALPATH_FORMS_TMP
    CriticalPath.ui
    ${CMAKE_SOURCE_DIR}/src/interface/ui/interval_select.ui
    ${CMAKE_SOURCE_DIR}/src/interface/ui/main_window.ui
    ${CMAKE_SOURCE_DIR}/src/interface/ui/node_select.ui
    ${CMAKE_SOURCE_DIR}/src/interface/ui/settings.ui
)

qt5_wrap_ui(CRITICALPATH_FORMS
  ${CRITICALPATH_FORMS_TMP}
  )

include_directories(${CMAKE_BINARY_DIR}/plugins/CriticalPath)
include_directories(${CMAKE_SOURCE_DIR}/src)
include_directories(${CMAKE_BINARY_DIR}/src)

add_library(CriticalPath SHARED ${CRITICALPATH_SRCS} ${CRITICALPATH_FORMS})

target_link_libraries(CriticalPath
  Qt5::Widgets
  Qt5::Core
  Qt5::Xml
  Qt5::OpenGL
  Qt5::UiTools
  )

set(Boost_USE_STATIC_LIBS OFF)
set(Boost_USE_MULTITHREADED ON)
set(Boost_USE_STATIC_RUNTIME OFF)
find_package(Boost 1.65 REQUIRED COMPONENTS graph)
message(STATUS "Boost version: ${Boost_VERSION}")

target_link_libraries(CriticalPath
  ${QT_LIBRARIES}
  ${OPENGL_gl_LIBRARY}
  ${OPENGL_glu_LIBRARY}
  ${Boost_LIBRARIES}
  )

if(${CMAKE_SYSTEM_NAME} MATCHES "Linux")
  target_link_libraries(CriticalPath
    rt
    )
  add_definitions("-DBOOST_GZIP")
endif()

if(VITE_ENABLE_VBO)
  target_link_libraries(CriticalPath
    ${GLEW_LIBRARY}
    )
endif()

install(TARGETS CriticalPath DESTINATION $ENV{HOME}/.vite)
